.PHONY: gen

init:
	GO111MODULE=on GOBIN=/usr/local/bin go get \
      github.com/bufbuild/buf/cmd/buf \
      github.com/bufbuild/buf/cmd/protoc-gen-buf-breaking \
      github.com/bufbuild/buf/cmd/protoc-gen-buf-lint

gen:
	buf generate --path test_assignment/meterusage
