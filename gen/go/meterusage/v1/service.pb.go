// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.17.3
// source: meterusage/v1/service.proto

package meterusage

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_meterusage_v1_service_proto protoreflect.FileDescriptor

var file_meterusage_v1_service_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x75, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x76, 0x31, 0x2f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1d, 0x74,
	0x65, 0x73, 0x74, 0x5f, 0x61, 0x73, 0x73, 0x69, 0x67, 0x6e, 0x6d, 0x65, 0x6e, 0x74, 0x2e, 0x6d,
	0x65, 0x74, 0x65, 0x72, 0x75, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x76, 0x31, 0x1a, 0x1c, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x6e, 0x6e, 0x6f, 0x74, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x22, 0x6d, 0x65, 0x74, 0x65,
	0x72, 0x75, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x76, 0x31, 0x2f, 0x72, 0x70, 0x63, 0x5f, 0x6d, 0x65,
	0x74, 0x65, 0x72, 0x75, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0xb5,
	0x01, 0x0a, 0x0a, 0x4d, 0x65, 0x74, 0x65, 0x72, 0x75, 0x73, 0x61, 0x67, 0x65, 0x12, 0xa6, 0x01,
	0x0a, 0x15, 0x4c, 0x69, 0x73, 0x74, 0x45, 0x6c, 0x65, 0x63, 0x74, 0x72, 0x69, 0x63, 0x69, 0x74,
	0x79, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x12, 0x35, 0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x61,
	0x73, 0x73, 0x69, 0x67, 0x6e, 0x6d, 0x65, 0x6e, 0x74, 0x2e, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x75,
	0x73, 0x61, 0x67, 0x65, 0x2e, 0x76, 0x31, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x45, 0x6c, 0x65, 0x63,
	0x74, 0x72, 0x69, 0x63, 0x69, 0x74, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x36,
	0x2e, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x61, 0x73, 0x73, 0x69, 0x67, 0x6e, 0x6d, 0x65, 0x6e, 0x74,
	0x2e, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x75, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x76, 0x31, 0x2e, 0x4c,
	0x69, 0x73, 0x74, 0x45, 0x6c, 0x65, 0x63, 0x74, 0x72, 0x69, 0x63, 0x69, 0x74, 0x79, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x1e, 0x82, 0xd3, 0xe4, 0x93, 0x02, 0x18, 0x12, 0x16,
	0x2f, 0x76, 0x31, 0x2f, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x72, 0x69, 0x63, 0x69, 0x74, 0x79, 0x2d,
	0x76, 0x61, 0x6c, 0x75, 0x65, 0x73, 0x42, 0x3b, 0x5a, 0x39, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62,
	0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x78, 0x6f, 0x72, 0x69, 0x75, 0x6d, 0x2f, 0x67, 0x65, 0x6e, 0x67,
	0x72, 0x70, 0x63, 0x2f, 0x67, 0x65, 0x6e, 0x2f, 0x67, 0x6f, 0x2f, 0x6d, 0x65, 0x74, 0x65, 0x72,
	0x75, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x76, 0x31, 0x3b, 0x6d, 0x65, 0x74, 0x65, 0x72, 0x75, 0x73,
	0x61, 0x67, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_meterusage_v1_service_proto_goTypes = []interface{}{
	(*ListElectricityRequest)(nil),  // 0: test_assignment.meterusage.v1.ListElectricityRequest
	(*ListElectricityResponse)(nil), // 1: test_assignment.meterusage.v1.ListElectricityResponse
}
var file_meterusage_v1_service_proto_depIdxs = []int32{
	0, // 0: test_assignment.meterusage.v1.Meterusage.ListElectricityValues:input_type -> test_assignment.meterusage.v1.ListElectricityRequest
	1, // 1: test_assignment.meterusage.v1.Meterusage.ListElectricityValues:output_type -> test_assignment.meterusage.v1.ListElectricityResponse
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_meterusage_v1_service_proto_init() }
func file_meterusage_v1_service_proto_init() {
	if File_meterusage_v1_service_proto != nil {
		return
	}
	file_meterusage_v1_rpc_meterusage_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_meterusage_v1_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_meterusage_v1_service_proto_goTypes,
		DependencyIndexes: file_meterusage_v1_service_proto_depIdxs,
	}.Build()
	File_meterusage_v1_service_proto = out.File
	file_meterusage_v1_service_proto_rawDesc = nil
	file_meterusage_v1_service_proto_goTypes = nil
	file_meterusage_v1_service_proto_depIdxs = nil
}
