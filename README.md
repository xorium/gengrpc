# gengrpc

Contians `.proto` files and generated stubs (servers, clientts, structures, classes etc).

# How To
## Installing required tools ([buf](https://github.com/bufbuild/buf)):
```bash
sudo make
```
## (Re)generating code:
```bash
make gen
```